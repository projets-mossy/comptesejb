package metier;

    import java.util.List;
	import javax.ejb.Remote;
	
	import metier.entities.Compte;
	@Remote
	public interface BanqueRemote {
		public Compte addCompte(Compte cp);
		public List<Compte> consulterComptes();
		public Compte consulterCompte(Long code);
		public void verser(Long code, double mt);
		public void retirer(Long code, double mt);
		public void virement(Long c1, Long c2, double mt);
	}


