package metier;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import metier.entities.Compte;

/**
 * Session Bean implementation class BanqueEJBImpl
 */
@Stateless(name = "BK")
public class BanqueEJBImpl implements BanqueLocal, BanqueRemote {
	@PersistenceContext(unitName = "banqueEJB")
	private EntityManager em;

	@Override
	public Compte addCompte(Compte cp) {
		em.persist(cp);
		return cp;
	}

	@Override
	public List<Compte> consulterComptes() {
		Query req = em.createQuery("select c from Compte c");
		return req.getResultList();
	}

	@Override
	public Compte consulterCompte(Long code) {
		Compte cp = em.find(Compte.class, code);
		if (cp == null)
			throw new RuntimeException("Ce compte n'existe pas");
		return cp;
	}

	@Override
	public void verser(Long code, double mt) {
		Compte cp = this.consulterCompte(code);
		cp.setSolde(cp.getSolde() + mt);
		em.persist(cp);
	}

	@Override
	public void retirer(Long code, double mt) {
		Compte cp = this.consulterCompte(code);
		if (cp.getSolde() < mt)
			throw new RuntimeException("Solde insuffisant");
		cp.setSolde(cp.getSolde() - mt);

	}

	@Override
	public void virement(Long c1, Long c2, double mt) {
		retirer(c1, mt);
		verser(c2, mt);
	}

}
