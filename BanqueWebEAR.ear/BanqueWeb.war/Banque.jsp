<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Compte Bancaires</title>
</head>
<body>

	<div id="formRecherche">
		<form action="controleur" method="get">
			<table>
				<tr>
					<td>Code :</td>
					<td><input type="number" name="code" value="${code}"
						required="required"></td>
					<td>${errCode}</td>
					<td><input type="submit" name="action" value="Consulter"></td>
					<td><input type="submit" name="action" value="Tous Les comptes"></td>
				</tr>
			</table>
		</form>
	</div>
	<c:if test="${(compte!=null)||(mtMsgErr!=null)}">
		<div id="compte">
			<table>
				<tr>
					<td>Code:</td>
					<td>${compte.code}</td>
				</tr>
				<tr>
					<td>Solde:</td>
					<td>${compte.solde}</td>
				</tr>
				<tr>
					<td>Date Cr�ation:</td>
					<td>${compte.date_creation}</td>
				</tr>
			</table>
		</div>
		<div id="formOperations">
			<form action="controleur" method="get">
				<table>
					<tr>
						<td><input type="hidden" name="code" value="${code}"></td>
						<td><input type="number" name="montant" required="required"
							value="${montant}"></td>
						<td>${mtMsgERr}</td>
						<td><input type="submit" name="action" value="Verser"></td>
						<td><input type="submit" name="action" value="Retirer"></td>
					</tr>
				</table>
			</form>
		</div>
	</c:if>
	<c:if test="${comptes!=null}">
		<div id="listeComptes">
			<table border="1" width="80%">
				<tr>
					<th>CODE</th>
					<th>SOLDE</th>
					<th>DATE CREATION</th>
				</tr>
				<c:forEach items="${comptes}" var="cp">
					<tr>
						<td>${cp.code}</td>
						<td>${cp.solde}</td>
						<td>${cp.date_creation}</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</c:if>
	<div id="errors">${exception}</div>
</body>
</html>