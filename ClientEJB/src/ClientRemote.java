import javax.naming.Context;
import javax.naming.InitialContext;
import metier.BanqueRemote;
import metier.entities.Compte;

public class ClientRemote {

	public static void main(String[] args) {
		try {
			Context ctx = new InitialContext();
			String appName = "BanqueWebEAR";
			String moduleName = "banqueEJB";
			String beanName = "BK";
			String remoteInterface = BanqueRemote.class.getName();
			String name = "ejb:" + appName + "/" + moduleName + "/" + beanName + "!" + remoteInterface;

			BanqueRemote proxy = (BanqueRemote) ctx.lookup(name);
			proxy.addCompte(new Compte());
			proxy.addCompte(new Compte());
			proxy.addCompte(new Compte());

			Compte cp = proxy.consulterCompte(1L);
			System.out.println(cp.getSolde());
			/*
			 * proxy.verser(1L, 4000); proxy.retirer(1L, 3000);
			 */
			proxy.virement(1L, 5L, 100);
			System.out.println(cp.getSolde());
		} catch (Exception e) {

			e.printStackTrace();
		}

	};

}
