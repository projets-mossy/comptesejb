package web;

import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import metier.entities.Compte;
import metier.BanqueLocal;

@WebServlet(name = "cs", urlPatterns = { "/controleur" })
public class ControleurServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EJB
	private BanqueLocal metier;

	@SuppressWarnings("null")
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			String action = request.getParameter("action");
			if (action != null) {
				if (action.equals("Consulter")) {
					Long code = Long.parseLong(request.getParameter("code"));
					request.setAttribute("code", code);
					Compte cp = metier.consulterCompte(code);
					request.setAttribute("compte", cp);
				} else if (action.equals("Tous Les comptes")) {
					request.setAttribute("comptes", metier.consulterComptes());
				} else if ((action.equals("Verser")) || (action.equals("Retirer"))) {
					double montant = Double.parseDouble(request.getParameter("montant"));
					Long code = Long.parseLong(request.getParameter("code"));
					request.setAttribute("montant", montant);
					request.setAttribute("code", code);
					if (action.equals("Verser")) {
						metier.verser(code, montant);
					} else {
						metier.retirer(code, montant);
					}
					request.setAttribute("compte", metier.consulterCompte(code));
				}
			}

			else if (action.equals("virement")) {
				Long code1 = Long.parseLong(request.getParameter("code1"));
				Long code2 = Long.parseLong(request.getParameter("code2"));
				double montant = Double.parseDouble(request.getParameter("montant"));
				request.setAttribute("code1", code1);
				request.setAttribute("code2", code2);
				request.setAttribute("montant", montant);
				metier.retirer(code1, montant);
				metier.verser(code2, montant);
				request.setAttribute("compte1", metier.consulterCompte(code1));
				request.setAttribute("compte2", metier.consulterCompte(code2));
			}
		} catch (Exception e) {
			request.setAttribute("exception", e.getMessage());
		}
		request.getRequestDispatcher("Banque.jsp").forward(request, response);
	}
}